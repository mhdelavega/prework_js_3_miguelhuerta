//Cálculo obtención letra DNI

//numero
var numDNI = prompt("Introduzca el número de su DNI (sin letra)");
if (numDNI < 0 || numDNI > 99999999 || numDNI.length < 8) {
    console.log("El número proporcionado no es correcto");
    fakeFunctionThatEffectivelyStopsAllProcessingInJavaScript();//error para abortar de stackoverflow
}

//letra
var letraDNI = prompt("Introduzca ahora la letra de su DNI");
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T']; 

compruebaLetra = numDNI % 23; // número correspondiente a letra
letraComputada = letras[compruebaLetra]; //obtiene letra del array

if (letraComputada == letraDNI) {
    console.log("DNI Válido");
}
else{
    console.log("DNI Incorrecto");
}


