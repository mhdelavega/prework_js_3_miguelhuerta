//Comprobación DNI en terminal (no ejecuta en Chrome)

var readline = require('readline'); // inicializa modulo readline

var rl = readline.createInterface(  //crea interfaz entrada y salida texto
  process.stdin,
  process.stdout
);

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T']; // declaración array letras

rl.question('Introduzca el número de su DNI (sin letra): ', (numDNI) => { //comienza interacción

  if (numDNI < 0 || numDNI > 99999999 || numDNI.length < 8) { //comprueba que estén todos los dígitos y sean válidos
    console.log("El número proporcionado no es correcto");
    rl.close();                                               //finaliza interacción
  }
  else{             //si numDNI es correcto

    compruebaLetra = numDNI % 23; // posición en array correspondiente a la letra
    letraComputada = letras[compruebaLetra]; //obtiene letra del array 

    rl.question(compruebaLetra +' Introduzca ahora la letra de su DNI: ', (letraDNI) => { //siguiente interacción
      if (letraDNI == letraComputada) {
        console.log("DNI Válido");
        rl.close();                             //finaliza interacción
      }
      else{
        console.log("DNI Incorrecto");
        rl.close();                              //finaliza interacción
      }
    });
  }
});
